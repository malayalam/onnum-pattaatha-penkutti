#അപ്രത്യക്ഷരായ ബഹിരാകാശ സന്ദർശകർ#

സ്വീകരണത്തിനുള്ള തയ്യാറെടുപ്പുകൾ തകൃതിയായി നടക്കുന്നു.  ലാബുല്ലിയന്മാർ ഭൂമിയിൽ ഇറങ്ങാൻ
പോവുകയാണ്.  ഇത്രയും ദൂരെയുള്ള ഒരു നക്ഷത്രവ്യൂഹത്തിൽ നിന്ന് ഒരാളും നമ്മുടെ സൗരയൂഥം
സന്ദർശിച്ചിട്ടില്ല.  അവരുടെ വരവിനെക്കുറിച്ചുള്ള ആദ്യത്തെ സിഗ്നലുകൾ പ്ലൂട്ടോയിലാണ്
പിടിച്ചെടുത്തത്.  മൂന്നുദിവസം കൊണ്ട് ലോന്ദയിലെ റേഡിയോ വാനനിരീക്ഷണാലയവുമായുള്ള ബന്ധം
സ്ഥാപിച്ചെടുത്തു.

ലാബുല്ലിയന്മാർ ഇപ്പോഴും വളരെ ദൂരെയാണ്.  പക്ഷെ ഷെമിത്യവോ കോസ്മോഡ്രോം അവരെ
സ്വീകരിക്കാൻ പൂർണമായും തയാറായിക്കഴിഞ്ഞു.  റെഡ്റോസ് ഫാക്ടറിയിൽ നിന്നുള്ള പെണ്‍കുട്ടികൾ
പൂമാലകൾ കൊണ്ട് അവിടമൊരു പൂങ്കാവനമാക്കി.  കവികളുടെ ബിരുദാനന്തര ഡിപ്പാർട്ടുമെന്റിലെ
വിദ്യാർഥികൾ വരവേൽപ്പുഗാനങ്ങൾ രിഹേഴ്സിക്കൊണ്ടിരിക്കയാണ്.  എല്ലാ എംബസികൾക്കും സ്വീകരണ
പ്ലാറ്റ്ഫോറത്തിൽ അവരുടെ സ്ഥാനങ്ങൾ തിട്ടപ്പെടുത്തിയിട്ടുണ്ട്.  കറസ്പോണ്ടർമാര് രാത്രിയും
പകലും കോസ്മോഡ്രോം കന്റീനിൽ തന്നെ കഴിച്ചുകൂട്ടി.

ആലീസ് അക്കാലത്ത് വ്നൂക്കവൊയിലുള്ള കോട്ടേജിലായിരുന്നു താമസം.  ഹെർബേറിയത്തിന് ചെടികൾ
സംഭരിക്കുകയായിരുന്നു.  വാന്യയുടെക്കാൾ നല്ല ഒരെണ്ണം വേണം അവൾക്ക്.  കിന്റർഗാർട്ടനിൽ
അവളുടെ ഒരുകൊല്ലം സീനിയറാണ് വാന്യ.  അതുകൊണ്ട് ലാബുല്ലിയന്മാരെ സ്വീകരിക്കാനുള്ള
തയ്യാറെടുപ്പുകളിലൊന്നും അവൾ പങ്കുകൊണ്ടില്ല.  വാസ്തവത്തിൽ അവരുടെ വരവിനെക്കുറിച്ചു പോലും
അവൾക്ക് അറിയാമായിരുന്നില്ല.

എനിക്കും അതുമായി നേരിട്ട് ബന്ധമൊന്നുമില്ല.  അവർ ഇവിടെ എത്തിയതിനുശേഷമേ എനിക്ക്
എന്തെങ്കിലും ചെയ്യാനുള്ളു.

അതിനിടക്ക് സംഗതികൾ നടന്നത് ഇപ്രകാരമാണ്: മാർച്ച് 8-ന് ലാബുല്ലിയന്മാർ അറിയിച്ചു, തങ്ങൾ
വർത്തുള കക്ഷയിൽ പ്രവേശിക്കാൻ പോവുകയാണെന്ന്.  ഏതാണ്ട് അതേ സമയത്ത് ആ ദുരന്തം സംഭവിച്ചു.
റാഡാർ സ്റ്റേഷൻ ലാബുല്ലിയൻ കപ്പലിനു പകരം ഒരു സ്വീഡിഷ് ഉപഗ്രഹവുമായി ബന്ധപ്പെട്ടു.
രണ്ടുകൊല്ലം മുമ്പ് നഷ്ടപ്പെട്ട 'നോബിൾ-29' ആയിരുന്നു അത്.  തെറ്റ് കണ്ടുപിടിച്ച്
തിരുത്തിയപ്പോഴേക്കും ലാബുല്ലിയന്മാരുമായുള്ള ബന്ധം നഷ്ടപ്പെട്ടു കഴിഞ്ഞിരുന്നു.  ബന്ധം
നഷ്ടപ്പെട്ട സമയത്ത് അവർ ഭൂമിയിലിറങ്ങാൻ തയ്യാറെടുത്തുകൊണ്ടിരിക്കയായിരുന്നു.

മാർച്ച് 9 കാലത്ത് 6:33ന് ലാബുല്ലിയന്മാർ വിളിച്ചു.  തങ്ങൾ ഭൂമിയിൽ ഇറങ്ങിയിരിക്കുന്നു.
ഭൂമിയിലെ അക്ഷാംശം 55° 20' വടക്കും രേഖാംശം 37° 40' കിഴക്കുമായ ഒരു സ്ഥാനത്താണ്
വന്നുപെട്ടിരിക്കുന്നത്, പരമാവധി 15'ന്റെ പിശകുണ്ടാകാം -- അതായത്, മോസ്കോവിൽ നിന്ന് ഏറെ
ദൂരെയല്ലാതെ.

വീണ്ടും ബന്ധം അറ്റുപോയി.  എത്ര ശ്രമിച്ചിട്ടും കിട്ടുന്നില്ല.  ഭൂമിയിൽനിന്നുള്ള വികിരണം
ലാബുല്ലിയൻ ഉപകരണങ്ങളെ വല്ലാതെ കേടുവരുത്തിയെന്നു തോന്നുന്നു.

ഒരായിരം കാറുകൾ നാലുപുറത്തേക്കും പായാൻ തുടങ്ങി, വ്യാപകമായ തെരച്ചിൽ ആരംഭിച്ചു.
ഹെലികോപ്റ്റർ, ഹോവർക്രാഫ്റ്റ്, ഓർണിത്തോപ്ടർ, ചുഴലിമാന്തി, പ്രൊപ്പോവിങ്... എന്നു വേണ്ട,
പറക്കുന്ന തരത്തിലുള്ള എന്തെല്ലാം യന്ത്രങ്ങളുണ്ടോ അവയെല്ലാം മാനത്തേക്ക് പൊന്തി.
മോസ്കോനഗരത്തിനു മേൽ ഇവ വെട്ടുകിളിപ്പറ്റത്തെപ്പോലെ കാണപ്പെട്ടു.  ആകാശം ഇരുണ്ടപോലിരുന്നു.
ലാബുല്ലിയന്മാർ ചുറ്റുവട്ടത്ത് എവിടെയായാലും, എന്തിന്, ഭൂമിക്കടിയിലാണെങ്കിൽ പോലും
കണ്ടുപിടിക്കപ്പെടേണ്ടതാണ്.

പക്ഷെ, അവരുടെ അഡ്രസില്ല.

ചുറ്റുവട്ടത്ത് താമസിക്കുന്ന ആരുംതന്നെ സ്പേസ്ഷിപ്പ് താഴത്ത് വരുന്നത് കണ്ടിട്ടില്ല.  മോസ്കോവിലും
പരിസരത്തുമുള്ള എല്ലാ മനുഷ്യരും, കൊച്ചുകുട്ടികൾ പോലും, അവർ ഇറങ്ങിയെന്നു പറഞ്ഞ സമയത്ത്
മാനത്തേക്ക് കണ്ണുനട്ടിരിക്കുകയായിരുന്നു.

എന്തോ അബദ്ധം പിണഞ്ഞിട്ടുണ്ട്, സംശയമില്ല.

വൈകുന്നേരം ഓഫീസിൽ നിന്ന് കോട്ടേജിലേക്ക് മടങ്ങി.  ഭൂമിയിലെ ദൈനംദിന ജീവിതം ആകെ
ക്രമം തെറ്റിയിരിക്കുന്നു.  ആർക്കും ഒന്നിനും മൂഡില്ല.  സന്ദർശകർക്ക് എന്തെങ്കിലും അബദ്ധം
പറ്റിയിരിക്കുമോ -- അതാണ്‌ എല്ലാവരുടെയും പേടി.

"ഒരു വേള അവർ ആന്റിമാറ്റർ കൊണ്ടുണ്ടാക്കിയതാവാം.  ഭൂമിയിലെ അന്തരീക്ഷത്തിൽ
പ്രവേശിച്ചപ്പോൾ അപ്രത്യക്ഷമായിക്കാണും."  മോണോറെയിൽ ട്രെയിനിൽ ഒരാൾ തട്ടിവിട്ടു.

"എന്ത്, ഒരു സ്ഫോടനവും കൂടാതെയോ?  ഒരു തെളിവും അവശേഷിപ്പിക്കാതെയോ?  സാധ്യമല്ല!"

"പക്ഷെ, ആന്റിമാറ്ററിന്റെ ഗുണധർമങ്ങളെക്കുറിച്ച് നമുക്കെന്തറിയാം."

"ഞങ്ങൾ ഭൂമിയിലിറങ്ങിയിരിക്കുന്നു എന്ന റേഡിയോ സന്ദേശം പിന്നെ ആരാ അയച്ചത്."

"ആരെങ്കിലും തമാശക്ക് ചെയ്തതായിരിക്കും."

"ഇങ്ങനത്തെ തമാശയോ!  ഈ സമയത്തോ."

"അപ്പോ പ്ലൂട്ടോയുമായി സംസാരിച്ചതും അയാളായിരിക്കുമോ."

"ഒരുവേള അവർ അദൃശ്യരായിരിക്കാം."

"പക്ഷെ, നമ്മുടെ റേഡിയോ ലൊക്കേറ്റർ കണ്ടുപിടിക്കാതിരിക്കില്ല..."

തർക്കം തുടർന്നു.  അവർ അദൃശ്യരായിരിക്കുമെന്നുള്ള സിദ്ധാന്തത്തിനാണു ഭൂരിപക്ഷം കിട്ടിയത്.

ഞാൻ അതേപ്പറ്റി ചിന്തിച്ചുകൊണ്ട് വരാന്തയിലിരുന്നു.  അവർ ഇവിടെ തൊട്ടടുത്താണ്
ഇറങ്ങിയിട്ടുള്ളതെന്ന് വരരുതോ.  കഷ്ടം, സാധുക്കൾ.  എന്തുകൊണ്ടാണ് ഈ ഭൂമിയിലുള്ളവർ തങ്ങളെ
അന്വേഷിച്ചു വരാത്തത്, അവരെ അവഗണിക്കുന്നതെന്തുകൊണ്ടാണ് എന്നൊക്കെ അവർ
അദ്ഭുതപ്പെടുകയായിരിക്കും.  ഒരു വേള അവർ പരിഭവിച്ച് തിരിച്ച് പറക്കാൻ
തുടങ്ങുകയായിരിക്കും... ഞാൻ പുറത്തിറങ്ങി പാടത്തേക്ക് പുറപ്പെടാൻ തുടങ്ങുകയായിരുന്നു.
അപ്പോഴാണ്‌ കാട്ടിൽ നിന്ന് ഒരു സെറ്റ് ആളുകൾ പുറത്തുവരുന്നത്‌ കണ്ടത്.  അവർ തപ്പിതപ്പിക്കൊണ്ട്
നടക്കുകയാണ്.  കാണാൻ പറ്റാത്തവരെ തൊടാനെങ്കിലും പറ്റിയെങ്കിലോ
എന്നാശിച്ചുകൊണ്ടായിരിക്കണം!

അതാ, പെട്ടെന്ന് ലോകത്തുള്ള എല്ലാ റേഡിയോ നിലയങ്ങളും കൂടി ആയിരം നാക്കുകളോടെ
സംസാരിക്കാൻ തുടങ്ങി.  വടക്കൻ ആസ്ത്രേലിയയിലെ ഒരു അമേച്വർ റേഡിയോസ്റ്റേഷൻ
പിടിച്ചെടുത്ത് ടേപ്പു ചെയ്ത ഒരു കമ്മ്യൂണിക്കെ ആയിരുന്നു അത്.  ലാബുല്ലിയന്മാരുടെ കമ്മ്യൂണിക്കെ.
നേരത്തെ പറഞ്ഞ അക്ഷാംശവും രേഖാംശവും വീണ്ടും ആവർത്തിച്ചു.  കമ്മ്യൂണിക്കെ തുടർന്നു: 'ഞങ്ങൾ
കാട്ടിലാണ്.  നിങ്ങളെ അന്വേഷിക്കാനായി ആദ്യത്തെ പാർട്ടിയെ അയച്ചിട്ടുണ്ട്.  ഞങ്ങൾ
നിങ്ങളുടെ പ്രക്ഷേപണത്തിന് ട്യൂണ്‍ ചെയ്തിരിക്കയാണ്.  പക്ഷെ, അദ്ഭുതമെന്നു പറയട്ടെ, ബന്ധം
കിട്ടുന്നില്ല...'  ഇവിടെ കമ്മ്യൂണിക്കെ മുറിയുന്നു.

അവർ അദൃശ്യജീവികളാണ് എന്ന പക്ഷക്കാരുടെ കൂട്ടത്തിൽ കുറെക്കൂടി ലക്ഷം ആളുകൾ കൂടി.

ലാബുല്ലിയന്മാരെ അന്വേഷിച്ചിറങ്ങിയ കൂട്ടർ കാട്ടിലേക്ക് വെച്ചടിച്ചു.  ആ സമയത്താണ് ആലീസ്
വാതിൽക്കൽ പ്രത്യക്ഷപ്പെട്ടത്.  അവളുടെ കയ്യിൽ ഒരു കുട്ടനിറയെ കാട്ടു സ്റ്റ്രാബറിപഴങ്ങളുണ്ട്.

"അവരൊക്കെ എന്തിനാ അങ്ങട്ടും ഇങ്ങട്ടും ഓടുന്നേ?  ഹല്ലോ പറയാനുംകൂടി നില്ക്കാതെ?"  അവൾ
ചോദിച്ചു.

"ആരാണ് ഈ 'അവർ'.  നീയാണ് 'ഹല്ലോ' പറയാത്തത്.  നിന്റെ അച്ഛനെ, ഒരേ ഒരച്ഛനെ കാലത്ത്
മുതൽ ഇതേവരെ നീ കണ്ടില്ലല്ലോ."

"ഇന്നലെ രാത്രിക്കുശേഷം!  അച്ഛൻ കാലത്തു പോകുമ്പോഴും ഞാൻ ഉറങ്ങുകയായിരുന്നല്ലോ.  പക്ഷെ,
എന്തിനാ ആളുകൾ കിടന്നോടുന്നത്?"

"ലാബുല്ലിയന്മാരെ കാണാനില്ല"  ഞാൻ മറുപടി പറഞ്ഞു.

"എനിക്കവരെ അറിയില്ല."

"ഇതുവരെ ആർക്കും അവരെ അറിയില്ല..."

"എങ്കിൽപിന്നെ അവരെ കാണാണ്ടായതെങ്ങനെ?"

"അവർ ഭൂമിയിലേക്ക് പറന്നുവരികയായിരുന്നു.  ഭൂമിയിലിറങ്ങുകയും ചെയ്തു.  അതിനുശേഷം
അഡ്രസില്ല."

ഞാൻ എന്തൊക്കെയോ വിഡ്ഢിത്തം പുലമ്പുകയാണെന്നു തോന്നി.  പക്ഷെ, സത്യം അതാണല്ലൊ.

ആലീസ് സംശയത്തോടെ എന്നെ നോക്കി.

"ഇങ്ങനെയൊക്കെ സംഭവിക്കുമോ?"

"ഇല്ല, സാധാരണനിലയിൽ ഇല്ല."

"പക്ഷെ, അവർക്ക് കോസ്മോഡ്രോം കാണാൻ പറ്റില്ലേ."

"പറ്റിയിട്ടില്ലായിരിക്കണം."

"എവിടെയാണ് അവരെ കാണാതായത്."

"മോസ്കോവിനടുത്ത് എവിടെയോ?  ഒരുവേള നമ്മുടെ ഈ കൊട്ടേജിൽനിന്ന് ഏറെ ദൂരെ ആയിരിക്കില്ല."

"ഹെലികോപ്ടറിലും കാൽനടയായിട്ടും ഒക്കെ ആളുകൾ അവരെ തിരക്കുകയാണ് അല്ലേ?"

"അതെ."

"എന്താ, അവർക്ക് നേരിട്ട് നമ്മുടെ അടുത്തേക്ക് വന്നുകൂടെ?"

"ഒരു വേള നമ്മൾ അങ്ങോട്ട് ചെല്ലുന്നതും കാത്തിരിക്കുകയായിരിക്കും.  അവർ ആദ്യമായല്ലേ
ഭൂമിയിൽ വരുന്നത്.  സ്പേസ്ഷിപ്‌ വിട്ടിറങ്ങാൻ ഭയമായിരിക്കും."

ആലീസ് നിശ്ശബ്ദയായി.  എന്റെ ഉത്തരംകൊണ്ട് തൃപ്തിപ്പെട്ടെന്നവണ്ണം അവൾ എന്തോ മനോരാജ്യം
വിചാരിച്ചുകൊണ്ട് വരാന്തയിൽ അങ്ങോട്ടും ഇങ്ങോട്ടും രണ്ടുചാൽ നടന്നു.  കയ്യിലുള്ള
സ്റ്റ്രാബറിക്കുട്ട നിലത്തുവെക്കാതെ.  പെട്ടെന്നവൾ ചോദിച്ചു.

"അവർ കാട്ടിലാണോ, പാടത്താണോ?"

"കാട്ടിൽ."

"എങ്ങനെ അറിയാം?"

"അവർതന്നെ പറഞ്ഞു.  റേഡിയോവിൽകൂടെ."

"നല്ലത്."

"എന്തു നല്ലത്?"

"അവർ പാടത്തല്ല എന്നത്."

"അതിലെന്താ ഇത്ര നല്ലതുള്ളത്?"

"ഞാനവരെ കണ്ടോ എന്ന് സംശയിച്ചുപോയി."

"അതെങ്ങനെയാ?"

"ഏയ്‌ ഒന്നുമില്ല.  ഞാൻ വെറുതെ പറയായിരുന്നു."  ഞാൻ പെട്ടെന്ന് ചാടി എണീറ്റു.
എനിക്കെന്റെ ആലീസിനെ അറിയാം.

"ഇല്ല, അച്ഛാ, സത്യമാണ്.  ഞാൻ കാട്ടിലേക്ക് പോയിട്ടേ ഇല്ല.  സത്യം.  സത്യം.  ഞാൻ
പാടത്ത് പോയതാ.  ഞാനവരെ കണ്ടില്ല.  സത്യം."

"ആലീസ് സത്യം പറ.  നീ കണ്ടതൊക്കെ പറ.  നിന്റെ വക പൊടിപ്പും തൊങ്ങലും ഒന്നും ചേർക്കണ്ട.
കാട്ടിൽ അസാധാരണമായി നീ എന്തെങ്ങിലും കണ്ടോ?"

"ഞാൻ സത്യമാ പറയുന്നതച്ഛാ.  ഞാൻ കാട്ടിലേക്ക് പോയിട്ടില്ല."

"ശരി എങ്കിൽ പാടത്ത്."

"ഞാനൊരു തെറ്റും ചെയ്തിട്ടില്ല.  അവരെ കണ്ടാൽ അസാധാരണമായി ഒന്നും തോന്നേം ഇല്ല."

ഞാനാകെ ഉദ്വിഗ്നനായി.

"ആലീസ്, സത്യം പറ.  വളച്ചുകെട്ടാതെ.  എവിടെ വെച്ചായിരുന്നു?  എന്താ നീ കണ്ടത്?  എന്നെ
വിഷമിപ്പിക്കാതെ.  മാനവസമൂഹത്തെയാകെ പീഡിപ്പിക്കാതെ."

"അച്ഛനാണോ മാനവസമൂഹം?"

"നോക്ക് ആലീസ്..."

"ശരി, ശരി.  അവരിവിടെത്തന്നെയുണ്ട്.  ഇതാ, അവരെന്റെ കൂടെവന്നു."

ഞാൻ തിരിഞ്ഞുനോക്കി.  വരാന്തയിൽ ആരുമില്ലായിരുന്നു.

"അച്ഛാ, അവിടെയല്ലച്ഛാ."  ആലീസ് നെടുവീർപ്പിട്ടുകൊണ്ട് എന്റെ അടുത്തുവന്നു.  "ഞാൻ അവരെ
എന്റെകൂടെ നിർത്താമെന്ന് വിചാരിച്ചു.  മാനവസമൂഹം മുഴുവൻ അവരെ
തിരക്കിക്കൊണ്ടിരിക്കയാണെന്ന് ഞാനറിഞ്ഞില്ല അച്ഛാ."

അവൾ സ്റ്റ്രാബറി നിറച്ച ആ കുട്ടാ എന്റെ കയ്യിൽ തന്നു.  ങേ!  ഇതെന്താണ്?  അവിശ്വസനീയം.
അതിനകത്തതാ രണ്ടു കൊച്ചുജീവികൾ സ്പേസ് സൂട്ടും അണിഞ്ഞുകൊണ്ട്.  അവരുടെ മേലെല്ലാം
സ്ട്രാബറിച്ചാറ് പുരണ്ടിരിക്കുന്നു.

"ഞാൻ അവരെ ഒന്നും ചെയ്തിട്ടില്ല."  ആലീസ് തെല്ല് കുറ്റബോധത്തോടെ പറഞ്ഞു.  "ഞാൻ
വിചാരിച്ചു അവ കൊച്ചു കുട്ടിച്ചാത്തൻമാരാണ് എന്ന്."

ഞാൻ അവൾ പറയുന്നതൊന്നും ശ്രദ്ധിക്കുന്നില്ലായിരുന്നു.  കുട്ട കയ്യിലെടുത്തുകൊണ്ട് ഞാൻ
വീഡിയോഫോണിന്റെ സമീപത്തേക്ക് ഓടി.  ഇവർക്ക് പുൽക്കൊടികൾ വൻ വൃക്ഷങ്ങളായി തോന്നിയതിൽ
അദ്ഭുതപ്പെടാനില്ല.

അങ്ങനെയാണ് ലാബുല്ലിയന്മാരുമായുള്ള ആദ്യ ബന്ധം സ്ഥാപിച്ചത്.


