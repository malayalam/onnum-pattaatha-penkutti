"ഒന്നും പറ്റാത്ത പെണ്‍കുട്ടി " is Malayalam translation of
[Kiril Bulychyev](https://en.wikipedia.org/wiki/Kir_Bulychov)'s
scifi/fantasy stories.

(English title: "The Girl Nothing Happens To: Adventures of
21st-century Alice - Told by Her Father.")

My 1984 copy, published by Social Scientist Pres, Trivandrum, is in
tatters and literally crumbling on touch.  I thought this book should
somehow live on, and so I'm trying to preserve this the way I know.  I
will post the scanned pages when I can.  Help is most welcome!

This effort is not authorized by the author or the translator.

I found an English translation
[here](http://www.arvindguptatoys.com/arvindgupta/27r.pdf).

